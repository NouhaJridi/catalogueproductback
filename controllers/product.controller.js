const db = require("../models");
const Product = db.products;
const Op = db.Sequelize.Op;

// Create and Save a new product
exports.create = (req, res) => {
    // Validate request
  console.log(req.body.institution);
  if (!req.body.institution || !req.body.institutionCode || !req.body.productCode || !req.body.productName || !req.body.productLevel) {
    res.status(400).send({
      message: "Content can not be empty!!"
    });
    return;
  }

  // Create a Product
  const product = {
    institution: req.body.institution,
    institutionCode: req.body.institutionCode,
    productCode: req.body.productCode ,
    productName: req.body.productName ,
    productLevel: req.body.productLevel ,
    derivation: req.body.derivation ,
    description: req.body.description ,
    duplication: req.body.duplication ,
    productType: req.body.productType ,
    version: req.body.version ,
    parentInstitution: req.body.parentInstitution ,
    parentProductCode: req.body.parentProductCode ,
    parentProductName: req.body.parentProductName ,
    parentProductVersion: req.body.parentProductVersion ,
    validityStartDate: req.body.validityStartDate ,
    validityEndDate: req.body.validityEndDate ,
    status:req.body.status,
    

  };

  // Save Product in the database
  Product.create(product)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Product."
      });
    });
};

