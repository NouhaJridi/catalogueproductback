module.exports = app => {
  const products = require("./controllers/product.controller.js");

  var router = require("express").Router();

  // Create a new Product
  router.post("/", products.create);


  app.use('/api/products', router);
};