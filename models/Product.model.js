module.exports = (sequelize, Sequelize) => {
  const Product = sequelize.define("product", {
    institution: {
      type: Sequelize.STRING
    },
    institutionCode: {
      type: Sequelize.STRING
    },
    productCode: {
      type: Sequelize.STRING
    },
    productName: {
      type: Sequelize.STRING
    },
    productLevel: {
      type: Sequelize.STRING
    },  
    derivation: {
      type: Sequelize.STRING
    }, 
    description: {
      type: Sequelize.STRING
    }, 
    duplication: {
      type: Sequelize.STRING
    }, 
    parentInstitution: {
      type: Sequelize.STRING
    }, 
    parentProductCode: {
      type: Sequelize.STRING
    }, 
    parentProductName: {
      type: Sequelize.STRING
    }, 
    parentProductVersion: {
      type: Sequelize.STRING
    }, 
  productType: {
      type: Sequelize.STRING
    }, 
  status: {
      type: Sequelize.STRING
    }, 
  validityEndDate: {
      type: Sequelize.DATE
    }, 
  validityStartDate: {
      type: Sequelize.DATE
    }, 
  version: {
      type: Sequelize.STRING
    },   
  statusDate: {
      type: Sequelize.DATE
    }, 
  });
 
  return Product;
};